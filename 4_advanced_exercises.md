# Exercises advanced concepts

## Part 1: Majoranas


Let's copy over some code from the lecture: just execute these cells right away.

```python
import numpy as np
from matplotlib import pyplot
import kwant
import kwant.continuum

import ipywidgets
from tqdm.notebook import tqdm

pi = np.pi

sigma_0 = np.eye(2)
sigma_x = np.array(
    [[0, 1],
     [1, 0]]
)
sigma_y = np.array(
    [[0, -1j],
     [1j, 0]]
)
sigma_z = np.array(
    [[1, 0],
     [0, -1]]
)

# Useful operators

phs = np.kron(sigma_y, sigma_y)
eh = -np.kron(sigma_0, sigma_z)  # Charge conservation, only there if Delta=0
```

```python
def interact(function, **params):
    params_spec = {
        key: ipywidgets.FloatText(value=value, step=0.05)
        for key, value in params.items()
    }
    return ipywidgets.interactive(function, **params_spec)

majorana_hamiltonian = """
    (k_x**2 / 2 - mu) * kron(sigma_0, sigma_z)
    + alpha * k_x * kron(sigma_y, sigma_z)
    + Delta * kron(sigma_0, sigma_x)
    + B_x * kron(sigma_x, sigma_0)
"""

wire_template = kwant.continuum.discretize(majorana_hamiltonian)
```

### Exercise 1: Majorana wave function

Previously we plotted the spectrum of the Majorana wire, but how do its eigenstates look?

```python
finite_wire = kwant.Builder()

def interval_shape(x_min, x_max):
    def shape(site):
        return x_min <= site.pos[0] < x_max
    
    return shape

finite_wire.fill(wire_template, shape=interval_shape(x_min=0, x_max=20), start=[10])

finite_wire = finite_wire.finalized()

def spectrum_finite(**params):
    kwant.plotter.spectrum(
        finite_wire,
        params=params,
        x=('B_x', np.linspace(0, 1)),
        show=False
    )
    pyplot.ylim(-.5, .5)

interact(
    spectrum_finite,
    mu=0.3,
    alpha=0.3,
    Delta=0.3,    
)
```

To get the eigenstates we need to compute the eigenvalue decomposition by hand—the convenience function `kwant.plotter.spectrum` isn't enough.

```python
# Choose the correct parameters for topological regime here.

h = finite_wire.hamiltonian_submatrix(
    params=dict(B_x=, mu=, alpha=, Delta=)
)

vals, vecs = np.linalg.eigh(h)
```

```python
# Solution

# Choose the correct parameters for topological regime here.

h = finite_wire.hamiltonian_submatrix(
    params=dict(B_x=0.6, mu=.3, alpha=0.5, Delta=0.3)
)

vals, vecs = np.linalg.eigh(h)
```

Now we are ready to plot the wave function.

**Plot wave function and charge density of the eigenstate closest to zero energy.**

```python
# While it's possible to use eigenvectors directly, let's instead use
# kwant.operator.Density instead

density = ...
charge = ...

lowest_eigenstate = 

pyplot.plot(...)
```

```python
# Solution

density = kwant.operator.Density(finite_wire)
charge = kwant.operator.Density(finite_wire, onsite=np.kron(sigma_0, sigma_z))

# Lowest energy state is halfway across the spectrum.
lowest_eigenstate = vecs[:, len(vecs) // 2]

pyplot.plot(density(lowest_eigenstate))
pyplot.plot(charge(lowest_eigenstate))
```

Do you see that when two Majoranas don't overlap, their wave function becomes neutral?


### Exercise 2: nonlocal conductance

At the point of a topological transition in a finite system, the bulk gap closes and a single mode transmits perfectly.

Our goal is to observe this phenomenon.

Let's start from creating a finite wire and attaching two normal leads to it.

```python
finite_wire = kwant.Builder()
finite_wire.fill(wire_template, shape=interval_shape(x_min=0, x_max=20), start=[10])

lead = kwant.Builder(
    symmetry=kwant.TranslationalSymmetry([1]),
    particle_hole=phs,
    conservation_law=eh,
)

lead.fill(
    wire_template.substituted(Delta='Delta_lead'),
    shape=(lambda site: True),
    start=[0]
)

# Now use the wire and the lead to create the system.
# Then plot it and confirm that you get a wire with two leads
```

```python
# solution

finite_wire = kwant.Builder()
finite_wire.fill(wire_template, shape=interval_shape(x_min=0, x_max=20), start=[10])

lead = kwant.Builder(
    symmetry=kwant.TranslationalSymmetry([1]),
    particle_hole=phs,
    conservation_law=eh,
)

lead.fill(
    wire_template.substituted(Delta='Delta_lead'),
    shape=(lambda site: True),
    start=[0]
)

finite_wire.attach_lead(lead)
finite_wire.attach_lead(lead.reversed())

finite_wire = finite_wire.finalized()

kwant.plot(finite_wire);
```

Now let's plot nonlocal heat current and electrical conductance.

The heat current is proportional to $∼\sum T_{ee} + \sum T_{he}$ (electrons and holes both carry positive energy, while the electrical current is $∼\sum T_{ee} - \sum T_{he}$.

```python
params = dict(
    B_x=0.0,
    Delta=.3,
    Delta_lead=0,
    mu=.3,
    mu_lead=.3,
    alpha=.3,
)

# Let's compute all scattering matrices first.
fields = np.linspace(0.3, .7, 100)
smatrices = []

for params['B_x'] in fields:
    smatrices.append(kwant.smatrix(finite_wire, params=params))

# Now to heat and charge conductances
def heat_conductance(smatrix):
    return ...

def electric_conductance(smatrix):
    return ...

pyplot.plot(fields, [heat_conductance(smatrix) for smatrix in smatrices])
pyplot.plot(fields, [electric_conductance(smatrix) for smatrix in smatrices]);
```

```python
# Solution

params = dict(
    B_x=0.0,
    Delta=.3,
    Delta_lead=0,
    mu=.3,
    mu_lead=.3,
    alpha=.3,
)

# Let's compute all scattering matrices first.
fields = np.linspace(0.3, .7, 100)
smatrices = []

for params['B_x'] in fields:
    smatrices.append(kwant.smatrix(finite_wire, params=params))

# Now to heat and charge conductances
def heat_conductance(smatrix):
    return smatrix.transmission(1, (0, 0))

def electric_conductance(smatrix):
    return (
        smatrix.transmission((1, 0), (0, 1))
        - smatrix.transmission((1, 1), (0, 1))
    )

pyplot.plot(fields, [heat_conductance(smatrix) for smatrix in smatrices])
pyplot.plot(fields, [electric_conductance(smatrix) for smatrix in smatrices]);
```

You could observe that at the point of the phase transition (check whether it occurs at the right parameter values) the *heat* conductance is close to 0.5, while electrical conductance stays low. This is because the transmitted mode is neutral.


## Part 2: Topological insulators


You now have the opportunity to extend our simulations of the 2D quantum spin Hall effect! Y

First, we of course invite you again to play with the lecture notebook. On top of that, you can
1. introduce a strong pertubation at the edge, and see how the topological edge state goes around
2. make a quantum point contact and investigate how edge states are scattering into each other
3. set up a Hall-bar geometry and compute non-local conductances as measured in [this paper](https://arxiv.org/abs/0905.0365)


First, let's copy the relevant code from the lecture itself, setting up the Hamiltonian template for you

```python
Gamma_so = [[0, 0, 0, -1],
            [0, 0, +1, 0],
            [0, +1, 0, 0],
            [-1, 0, 0, 0]]

hamiltonian = """
   + M * kron(sigma_0, sigma_z)
   - B * (k_x**2 + k_y**2) * kron(sigma_0, sigma_z)
   - D * (k_x**2 + k_y**2) * kron(sigma_0, sigma_0)
   + A * k_x * kron(sigma_z, sigma_x)
   - A * k_y * kron(sigma_0, sigma_y)
   + Delta * Gamma_so
"""

params_bhz=dict(A=364.5, B=-686, D=-200, M=-10, Delta=1.6, Delta_lead=0)
```

```python
hamiltonian = kwant.continuum.sympify(hamiltonian, locals=dict(Gamma_so=Gamma_so))
hamiltonian
```

```python
lattice_spacing = 10
template = kwant.continuum.discretize(hamiltonian, grid=lattice_spacing)
```

### Exercise 1: Edge perturbation


A lot has been predefined for you in this exercise. You mainly need to decide which shape you would like to implement.

Let's build a system that has a perturbation at the edge. For example, you could cut a half-circle out of the edge.

```python
W = 100
L = 100

def disturbed_rectangle_shape(site):
    x, y = site.pos
    
    # Make this a proper shape function for the scattering region!

def lead_shape(site):
    x, y = site.pos
    
    # Make this a proper shape function for the leads
```

```python
#Solution
W = 100
L = 100
R = 15

def disturbed_rectangle_shape(site):
    x, y = site.pos
    # We make a shape of a rectangle, and cut out a circle centered at the lower bottom part
    return 0 < y < W and 0 < x < L and (x-L/2)**2 + y**2 > R**2

def lead_shape(site):
    x, y = site.pos
    return 0 < y < W
```

```python
syst = kwant.Builder()
syst.fill(template, disturbed_rectangle_shape, (lattice_spacing, lattice_spacing))

# We use the trick from the lecture setting Delta=0 in the leads
lead = kwant.Builder(kwant.TranslationalSymmetry([-lattice_spacing, 0]), conservation_law=np.diag([-1, -1, 1, 1]))
lead.fill(template.substituted(Delta='Delta_lead'), lead_shape, (0, lattice_spacing))

syst.attach_lead(lead)
syst.attach_lead(lead.reversed())

syst = syst.finalized()
kwant.plot(syst);
```

Now let us compute the conductance as a function of energy

```python
energies = np.linspace(-20, 20, 21)
Gs = []

for en in tqdm(energies):
    smat = kwant.smatrix(syst, energy=en, params=params_bhz)
    Gs.append(smat.transmission(1, 0))
    
pyplot.plot(energies, Gs)
pyplot.ylabel("G [$e^2/h$]")
pyplot.xlabel("energy [meV]")
```

We thus see that the conductance is still quantized in the gap! (with a little dip due to finite size quantization).
This is due to topological protection.

Let's look at what happens physically by checking the wave function at an energy inside gap (e.g. betwen -10meV and +10meV):

```python
density = kwant.operator.Density(syst, np.diag([1,1,-1,-1]))
wfs = kwant.wave_function(syst, energy=-5, params=params_bhz)

kwant.plotter.map(syst, density(wfs(0)[0]), cmap="seismic", vmin=-0.015, vmax=0.015)
kwant.plotter.map(syst, density(wfs(0)[1]), cmap="seismic", vmin=-0.015, vmax=0.015);
```

```python

```

### Exercise 2 - backscattering in a point contact


Now let's take a different system where we make the perturbation in the middle so strong that the two edge states are overlapping. In this case, there can be scattering between the two states


First define a proper geometry, for example by introducing a circular cutout at both sides:

```python
def qpc_shape(site):
    x, y = site.pos
    
    # Make this a proper shape function for the scattering region!

def lead_shape(site):
    x, y = site.pos
    
    # Make this a proper shape function for the leads
```

```python
#Solution

W = 100
L = 100
R = 30

def qpc_shape(site):
    x, y = site.pos
    # We make a shape of a rectangle, and cut out a circle centered at the lower bottom part
    return 0 < y < W and 0 < x < L and (x-L/2)**2 + y**2 > R**2 and (x-L/2)**2 + (y-W)**2 > R**2

def lead_shape(site):
    x, y = site.pos
    return 0 < y < W
```

```python
syst = kwant.Builder()
syst.fill(template, qpc_shape, (lattice_spacing, lattice_spacing))

# We use the trick from the lecture setting Delta=0 in the leads
lead = kwant.Builder(kwant.TranslationalSymmetry([-lattice_spacing, 0]), conservation_law=np.diag([-1, -1, 1, 1]))
lead.fill(template.substituted(Delta='Delta_lead'), lead_shape, (0, lattice_spacing))

syst.attach_lead(lead)
syst.attach_lead(lead.reversed())

syst = syst.finalized()
kwant.plot(syst);
```

First look at the wave functions, and let us vary the spin-orbit parameter to see what happens:

```python
n_wf=0

def plot_qpc_wf(energy, Delta):
    params = dict(params_bhz)   # make a copy
    params.update(Delta=Delta)
    
    density = kwant.operator.Density(syst, np.diag([1,1,-1,-1]))
    wfs = kwant.wave_function(syst, energy=energy, params=params)

    kwant.plotter.map(syst, density(wfs(0)[n_wf]), cmap="seismic", vmax=0.01, vmin=-0.01)
    
interact(
    plot_qpc_wf,
    energy = -5,
    Delta=1.6
    )
```

You should see how with increasing spin-orbit the edge state can be scattered between different spin states. Note if you increase spin-orbit too much, you will eventually kill the topological phase


Your task: Now let's quantify this picture in terms of how much transmission there is through the QPC to the "spin up" and "spin down" state on the right side.

To this end, make a plot of transmission to "spin up" and "spin down" as a function of spin-orbit strength Delta for some energy within the gap.

```python

```

```python
# Solution
energy = 0

Deltas = np.linspace(0, 10, 21)
T_up =[]
T_down =[]

for Delta in Deltas:
    params = dict(params_bhz)   # make a copy
    params.update(Delta=Delta)
    
    smat = kwant.smatrix(syst, energy=energy, params=params)
    T_up.append(smat.transmission((1, 0), (0, 0)))
    T_down.append(smat.transmission((1, 1), (0, 0)))
    
pyplot.plot(T_up, label="up")
pyplot.plot(T_down, label="down")
pyplot.ylabel("$T$")
pyplot.xlabel("$\Delta$")
pyplot.legend();
```

### Exercise 3 - non-local conductances in a Hall geometry


We will now try to measure non-local conductance in a Hall bar geometry, trying to reproduce quantized values as in [this paper](https://arxiv.org/pdf/0905.0365.pdf)


First, let's set up a Hall-bar geometry like that:

![image.png](images/hallbar1.png)


We will help you a little bit, let's already set up leads 0, 1, 2, 3

```python
W = 100
L = 500
offset = 50

def central_shape(site):
    x, y = site.pos
   
    return 0 < y < W and 0 < x < L

def lead01_shape(site):
    x, y = site.pos
    return 0 < y < W

def lead23_shape(site):
    x, y = site.pos
    return offset < x < offset + W

```

```python
syst = kwant.Builder()
syst.fill(template, central_shape, (lattice_spacing, lattice_spacing))

# For this problem it's not necessary to identify spins, so I keep it simpler
lead01 = kwant.Builder(kwant.TranslationalSymmetry([-lattice_spacing, 0]))
lead01.fill(template, lead01_shape, (0, lattice_spacing))

syst.attach_lead(lead01)
syst.attach_lead(lead01.reversed())

lead23 = kwant.Builder(kwant.TranslationalSymmetry([0, -lattice_spacing]))
lead23.fill(template, lead23_shape, (offset + lattice_spacing, 0))

syst.attach_lead(lead23)
syst.attach_lead(lead23.reversed())

kwant.plot(syst)
```

Now please add the leads 4 and 5 and finalize the system!

```python

```

```python
#Solution

def lead45_shape(site):
    x, y = site.pos
    return L-offset-W < x < L-offset

lead45 = kwant.Builder(kwant.TranslationalSymmetry([0, -lattice_spacing]))
lead45.fill(template, lead45_shape, (L-offset-lattice_spacing, 0))

syst.attach_lead(lead45)
syst.attach_lead(lead45.reversed())

syst = syst.finalized()

kwant.plot(syst)

```

For an energy inside the gap, we can now compute all the transmissions between all the leads. Kwant actually has a convenience function for this: `conductance_matrix`. This computes a matrix $G$ such that

$$ I = G V$$

where $I$ is a vector of currents through the different leads, and $V$ is a vector of voltages

```python
smat = kwant.smatrix(syst, energy=-5, params=params_bhz)
G = smat.conductance_matrix()
```

```python
print(np.round(G, 2))
```

Now let's say we want to simulate a situation in the experiment like this:

![image.png](images/hallbar2.png)


What is done here is that a certain current is imposed on the system, namely $-I$ in lead 0 and $I$ in lead 1 (the sign convention follows from how the conductance matrix is defined: outgoing current is positive). We would now like to know what voltages this corresponds to. 

Looking at $I = G V$ it seems the only thing we need to do is invert $G$. Try this!

```python
import scipy.linalg as la

```

```python
#Solution
import scipy.linalg as la

la.inv(G)
```

You will actually get a lot of (numerical) infinities! This is due to the fact that the system has conservation laws for current (Kirchhoff's law) and due to the fact that boltages are only defined up to an offset. An easy way out is to take one voltage as 0. Let's choose $V_0$ for that. Then we can simply remove $I_0$ and $V_0$ from the system of equations, meaning we remove row 0 and column 0 from the conductance matrix. Now we lost $I_0$, but we know that it must be equal to minus the sum of all other currents.

The remaining currents and voltages are described by a 5x5 system now, which is invertible. So let's now set our input current vector (note that V_0 is gone, so V_1 will be at index 0) and compute $V = G^{-1} I$:

```python
current = [1, 0, 0, 0, 0]

#compute V!
```

```python
#Solution

current = [1, 0, 0, 0, 0]

voltage = la.solve(G[1:, 1:], current)
```

With this it will now be easy for you to compute the quantities 

$$R_{14,nm} = (V_m - V_n)/I$$ 

from [the paper](https://arxiv.org/pdf/0905.0365.pdf)! Do you get the experimental values?

```python

```

```python
# Solution

# Note: I use our numbering of the leads
print("R_01,01:", voltage[0] - 0)
print("R_01,35", voltage[4] - voltage[2])
```

```python

```

```python

```
