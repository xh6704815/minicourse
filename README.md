# Kwant mini-course

Run the workshop contents interactively [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kwant-project.org%2Fkwant%2Fminicourse.git/master?filepath=index.md) or download a [zip archive](https://gitlab.kwant-project.org/kwant/minicourse/builds/artifacts/master/raw/qt_workshop_notebooks.zip?job=prepare%20archive).

## Instructors

- Christoph Groth
- Xavier Waintal
- Michael Wimmer
- Anton Akhmerov
- Thomas Kloss
- Dániel Varjas
- Bas Nijholt
- Pablo Piskunow
- Genevieve Fleury
- Abbout Adel
- Chatzikyriakou Eleni
- Adel Kara-Slimane
- Isidora Araya Day
- Branislav Nikolic
- Ousmane Ly
- Antonio Lucas Rigotti Manesco
- Michael Barth
- Jacob Fuchs
- Andreas Bereczuk

## Links

- [Workshop page](https://virtualscienceforum.org/#/quantum-transport-workshop)
- Virtual Science Forum [application](https://github.com/virtualscienceforum/virtualscienceforum/issues/215)
